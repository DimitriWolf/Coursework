from termcolor import colored
import os
import time
class Part_interface:

	@staticmethod
	def about_the_autor():
		os.system('clear')
		print(colored("                                 _ .--.           ","blue"))
		print(colored("                                ( `    )          ","blue"))
		print(colored("                             .-'      `--,        ","blue"))
		print(colored('                  _..----.. ','red')+colored(' (            )`-.    ','blue'))
		print(colored("                .'_|` _|` _|","red")+colored("(  .__,           )   ","blue"))
		print(colored("               /_|  _|  _|  _","red")+colored("(        (_,  .-'    ","blue"))
		print(colored("              ;|  _|  _|  _|  ","green")+colored("'-'__,--'`--'       ","blue"))
		print(colored("              | _|  _|  _|  _| |                  ","green"))
		print(colored("              |  _|  _|  _|  _|                   ","green"))
		print(colored("        _.--.","blue")+colored("  \_|  _|  _|  _|/                   ","yellow"))
		print(colored("     .-'       )--,","blue")+colored("|  _|  _|.`                    ","yellow"))
		print(colored("   (__, (_      ) )","blue")+colored("_|  _| / /      Create by      ","yellow"))
		print(colored("      `-.__.\ _,--'","blue")+colored("\|__|__/          Dimitry      ","yellow"))
		print(colored("                    ;____;            Kravchenko  ","blue"))
		print(colored("                     \YT/                <It-12-sp>","blue"))
		print(colored("                      ||                          ","red"))
		print(colored("                     !  !                         ","red"))
		print(colored("                     '=='                         ","red"))
		time.sleep(5)
		#os.system('clear')

	@staticmethod
	def form_menu(mashine_1, mashine_2, mashine_3):
		print(colored(" >>>>>>>>>>>>>>> <<<<<<<<<<<<<<< ","blue"))
		print(colored(" 1) ","green") + mashine_1 + colored(" (comand: ","blue") + colored(mashine_1,"blue") + colored(")","blue"))
		print(colored(" 2) ","green") + mashine_2 + colored(" (comand: ","blue") + colored(mashine_2,"blue") + colored(")","blue"))
		print(colored(" 3) ","green") + mashine_3 + colored(" (comand: ","blue") + colored(mashine_3,"blue") + colored(")","blue"))
		print(colored(" >>>>>>>>>>>>","blue") + colored(" 2018 ","green") + colored("<<<<<<<<<<<<< ","blue"))

	@staticmethod
	def top_hat():
		os.system('clear')
		print(colored(" ___          _      ___          ","green"))         
		print(colored("| _ \__ _ _ _| |__  / __|__ _ _ _ ","green"))
		print(colored("|  _/ _` | '_| / / | (__/ _` | '_|","yellow"))
		print(colored("|_| \__,_|_| |_\_\  \___\__,_|_|  ","yellow"))
	
	@staticmethod	
	def frame_the_reader():
		print(colored("=================================== ","blue") + colored("================================== ","blue"))
	@staticmethod
	def frame_the_input():
		print(colored(" ________ Write new item ________ ","yellow"))
	@staticmethod
	def frame_the_delete_item():
		print(colored(" ________ Delete item ________ ","red"))


