from Parck_mashine import Parck_mashine, Passenges, Trucks, Tractors, Buldozers, Watering_mashine, Read_Write
from Part_interface import Part_interface
from termcolor import colored
import os
class Input_output:
	#Функцiя, яка виводить данні із файла
	def use_item(type_mashine, file_csv):
		Part_interface().top_hat()
		Part_interface.frame_the_reader()
		descriptes = []
		descriptes = Read_Write.reader(file_csv)
		print(descriptes)
		Part_interface.frame_the_reader()


	#Функцiя, яка виконує запис данних у файл
	def use_command(file_csv, type_class):
		item_writer = []
		Part_interface().frame_the_input()
		item_writer = type_class().item_input()
		Read_Write.writer(item_writer, file_csv)
	

	@staticmethod #Функція виведення меню допомоги
	def help_menu(type_mashine):
		print(colored("Help menu:","yellow"))
		print("	>> add "+ type_mashine + colored(" -- add new item to list","blue"))
		print("	>> delete item" + colored(" -- removing an item","blue"))
		print("	>> back" + colored(" -- return to menu","blue"))
		print("	>> close" + colored(" -- exit from the program","blue"))

