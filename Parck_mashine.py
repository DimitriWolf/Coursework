from termcolor import colored
import pandas as pd
import csv
class Parck_mashine:	
	def __init__(self):
		self.model = input(colored("  > Get model (name) , ","green"))
		self.working_capacity = input(colored("  > Get engine capacity, l ","green"))
		self.max_speed = input(colored("  > Get max speed, km ","green"))

class Passenges(Parck_mashine):
	def __init__(self):
		super().__init__()
		print(" and, ")
		self.size = input(colored("  > Size (class A,B,C,M,J) ","yellow"))
	def item_input(self):	
		data_auto =  [[self.model, self.working_capacity, self.max_speed, self.size]]
		return (data_auto)

class Trucks(Parck_mashine):
	def __init__(self):
		super().__init__()
		print(" and, ")
		self.platform_capacity = input(colored("  > Platform capacity, kg ","yellow"))	
		self.maximum_load = input(colored("  > Maximum load, kg ","yellow"))
	def item_input(self):
		data_auto =  [[self.model, self.working_capacity, self.max_speed, self.platform_capacity, self.maximum_load]]
		return (data_auto)

#Special cars		
class Tractors(Parck_mashine):
	def __init__(self):
		super().__init__()
		print(" and, ")
		self.traction_class = input(colored("  > Traction class ","yellow"))
		self.full_mass = input(colored("  > Full mass, kd ","yellow"))	
	def item_input(self):
		data_auto =  [[self.model, self.working_capacity, self.max_speed, self.traction_class, self.full_mass]]
		return (data_auto)

class Buldozers(Parck_mashine):
	def __init__(self):
		super().__init__()
		print(" and, ")
		self.operating_weight = input(colored("  > Max power, kg ","yellow"))
		self.reted_power = input(colored("  > Reted power, w ","yellow"))	
	def item_input(self):
		data_auto =  [[self.model, self.working_capacity, self.max_speed, self.operating_weight, self.reted_power]]
		return (data_auto)

class Watering_mashine(Parck_mashine):
	def __init__(self):
		super().__init__()
		print(" and, ")
		self.tank_volume = input(colored("  > Tank volume, l ","yellow"))
		self.scattering_arrea = input(colored("  > Scattering, m2 ","yellow"))		
	def item_input(self):
		data_auto =  [[self.model, self.working_capacity, self.max_speed, self.tank_volume, self.scattering_arrea]]
		return (data_auto)

class Read_Write:
	@staticmethod
	def writer(data_auto, file_csv):
		with open(file_csv, 'a') as csvfile:
			csvwriter = csv.writer(csvfile)	
			csvwriter.writerows(data_auto)	

	@staticmethod	
	def reader(csv_file):
		descriptes = pd.read_csv(csv_file)
		return (descriptes)

	@staticmethod
	def deletes(row, csv_file):
		csv_file = csv_file + '_csv.csv'
		descriptes = pd.read_csv(csv_file)
		descriptes = descriptes.drop(descriptes.index[[row]])
		df_csv = descriptes.to_csv(csv_file, mode = 'w', index = False)
		print(colored("Ok","green"))
