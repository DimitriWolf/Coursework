from Parck_mashine import Parck_mashine, Passenges, Trucks, Tractors, Buldozers, Watering_mashine, Read_Write
from Part_interface import Part_interface
from Input_output import Input_output
from termcolor import colored
import os
class Check_input:
	input_type = ''
	@staticmethod #Функцiя повернення оновленої таблиці
	def return_table(type_mashine):
		Check_input.check_item(type_mashine)
		comand_2 = input(colored(">> Enter the command for " + type_mashine + " ", "green"))
		Check_input.check_command(input_comand)

	def check_item(type_mashine):
		global input_type
		input_type = type_mashine
		'''
			Che.executive_lasses(command, format("{}_csv.csv", command))
		''' 
		if (type_mashine == 'passenges'):
			Input_output.use_item(type_mashine, 'passenges_csv.csv')
		elif (type_mashine == 'trucks'):
			Input_output.use_item(type_mashine, 'trucks_csv.csv')
		elif (type_mashine == 'tractors'):
			Input_output.use_item(type_mashine, 'tractors_csv.csv')
		elif (type_mashine == 'buldozers'):
			Input_output.use_item(type_mashine, 'buldozers_csv.csv')
		elif (type_mashine == 'watering'):
			Input_output.use_item(type_mashine, 'watering_mashine_csv.csv')
		elif (type_mashine == 'special'):
			os.system('clear')
			Part_interface().top_hat()
			Part_interface().form_menu("Tractors", "Buldozers", "Watering")
			type_mashine = input(colored(">> Select item  ","green"))
			input_type = type_mashine
			Check_input.check_item(type_mashine)
		elif (type_mashine == 'close'):
			raise SystemExit(1)
		else:
			print(colored("Unknown item :c","red"))
			print(colored("  Try again ...","green"))
			type_mashine = input(colored(">> Select item  ","green"))
			Check_input.check_item(type_mashine)

		comand_2 = input(colored(">> Enter the command for " + type_mashine + " ", "green"))
		Check_input.check_command(comand_2)

	def check_command(input_comand):
		global input_type
		if (input_comand == 'add passenges'):
			Input_output.use_command('passenges_csv.csv', Passenges)
			Check_input.return_table(input_type)	
		elif (input_comand == 'add trucks'):
			Input_output.use_command('trucks_csv.csv', Trucks)
			Check_input.return_table(input_type)
		elif (input_comand == 'add tractors'):
			Input_output.use_command('tractors_csv.csv', Tractors)
			Check_input.return_table(input_type)
		elif (input_comand == 'add buldozers'):
			Input_output.use_command('buldozers_csv.csv', Buldozers)
			Check_input.return_table(input_type)
		elif (input_comand == 'add watering'):
			Input_output.use_command('watering_mashine_csv.csv', Watering_mashine)
			Check_input.return_table(input_type)
		elif (input_comand == "back"):
			os.system('clear')
			Part_interface().top_hat()
			Part_interface().form_menu("Passenges", "Trucks", "Special")
			type_mashine = input(colored(">> Select item ","green"))
			Check_input.check_item(type_mashine)
		elif (input_comand == 'delete item'):
			Part_interface().frame_the_delete_item()
			delete_row = int(input(colored(' Row or delete: ','green')))
			Read_Write.deletes(delete_row, input_type)
			Check_input.return_table(input_type)
		elif (input_comand == 'help'):
			Input_output.help_menu(input_type)
		elif (input_comand == 'close'):
			raise SystemExit(1)
		else:
			print(colored("I dont knov this comand ;c","red"))
			print(colored(" to get list of commands, enter >> help","red"))
			
		comand_2 = input(colored(">> Enter the command for " + input_type+ " ", "green"))
		Check_input.check_command(comand_2)


			


